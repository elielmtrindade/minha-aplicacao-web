FROM nginx:alpine
RUN apk update && \
    apk add --no-cache curl

RUN curl -L https://devopselegante.netlify.app -o /usr/share/nginx/html/devops_elegante

EXPOSE 80
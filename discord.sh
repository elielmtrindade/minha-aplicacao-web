WEBHOOK_URL= $WEBHOOK_URL

# Mensagem a ser enviada
MESSAGE="Pipeline concluida com sucesso!"

# Enviar a mensagem para o Discord via webhook
curl -H "Content-Type: application/json" -X POST -d "{\"content\":\"$MESSAGE\"}" $WEBHOOK_URL